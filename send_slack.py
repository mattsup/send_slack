#!/usr/bin/env python
# -*- coding: utf-8 -*-


import requests, json
import config_slack_data

## Init config vars
slack_webhook_url = config_slack_data.SLACK_WEBHOOK_URL
slack_icon_url = config_slack_data.ICON_URL
slack_username = config_slack_data.USERNAME

def SendSlack(text_input):
    ## Format the data in json to be displayed on Slack
    data = {"text": f"{text_input}", "username": f"{slack_username}"}
    json_data = json.dumps(data)

    ## Displays the formatted data for debug
    print(json_data)

    ## Sends the data to Slack
    slack_notification = requests.post(slack_webhook_url, json_data)

    ## Check the error code from the Slack API for debug
    print(slack_notification)

